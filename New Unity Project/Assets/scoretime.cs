﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoretime : MonoBehaviour
{
    float score = 0.0f;
    int minute = 0;
    GameObject ScoreTime;
    // Start is called before the first frame update
    void Start()
    {
        ScoreTime = GameObject.Find("scoreTime");
    }

    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime;
        if(score >= 59)
        {
            minute++;
        }
     
        ScoreTime.GetComponent<Text>().text =minute + "分" + score.ToString("F0") + "秒";
    }
}
