﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class insekiGenerator : MonoBehaviour
{
    public GameObject insekiPrefab;
    float span = 0.6f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if(delta > span)
        {
            delta = 0;
            GameObject go = Instantiate(insekiPrefab);
            int px = Random.Range(-4, 3);
                go.transform.position = new Vector3(10, px, 0);
            
        }
    }
}
