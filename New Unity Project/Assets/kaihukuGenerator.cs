﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kaihukuGenerator : MonoBehaviour
{
    public GameObject kaihukuPrefab;
    float span = 1.0f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if (delta > span)
        {
            delta = 0;
            GameObject go = Instantiate(kaihukuPrefab);
            int px = Random.Range(-4, 3);
            if (px % 2 == 0)
            {
                go.transform.position = new Vector3(10, px, 0);
            }
        }
    }
}
